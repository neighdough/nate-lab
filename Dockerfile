FROM ubuntu:latest
WORKDIR /usr/local/bin

ADD hello.sh /usr/local/bin
ADD hello_failure.sh /usr/local/bin 

ENTRYPOINT ["/usr/local/bin/hello.sh"]
CMD chmod 777 /usr/local/bin/hello.sh && chmod 777 /usr/local/bin/hello_failure.sh 



